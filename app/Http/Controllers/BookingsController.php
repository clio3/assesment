<?php

namespace App\Http\Controllers;

use App\DataTables\BookingsDataTable;

class BookingsController extends Controller
{
    public function index(BookingsDataTable $dataTable)
    {

        return $dataTable->render('bookings.my-bookings', [
            'breadcrumbs' => trans('application.titles.my_bookings'), //These would normally be taken and un-slugified from the route name
        ]);
    }
}
