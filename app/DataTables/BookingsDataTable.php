<?php

namespace App\DataTables;

use App\Models\Booking;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class BookingsDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param  QueryBuilder  $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->editColumn('created_at', function (Booking $booking) {
                return $booking->created_at->format('d M Y');
            })
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Booking $model): QueryBuilder
    {
        return $model->newQuery()->with(['customer', 'museum', 'user'])->select('bookings.*');
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('bookings-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('frtip')
            ->orderBy(1)
            ->selectStyleSingle()
            ->buttons([
                Button::make('print'),
            ])
            ->initComplete("function (settings, json) {
                this.api().columns().every(function (index) {
                    let column = this;
                    let input = document.createElement(\"input\");

                    let timeoutTimer;
                    $(input).appendTo($(column.footer())).on('input', function () {
                        timeoutTimer = setTimeout(() => {
                            column.search($(this).val(), false, false, true).draw();
                        }, 150)
                    });
                });
            }");

    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('user')->name('user.username')->data('user.username')->title('Agent Name'),
            Column::make('booking_id')->searchable(false),
            Column::make('customer')->name('customer.full_name')->data('customer.full_name')->title('Traveler\'s Full Name')->searchable(),
            Column::make('museum')->name('museum.name')->data('museum.name')->title('Museum Booked')->searchable(),
            Column::make('created_at')->title('Date')->searchable(),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Bookings_'.date('YmdHis');
    }
}
