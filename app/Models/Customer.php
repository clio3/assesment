<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Customer extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['first_name', 'last_name', 'full_name'];

    protected static function boot()
    {
        parent::boot();
        //This could also be an Attribute. The datatables implementation did not allow me to search on relatiosnip columns so this is my approach to make the customer searchable without too much hassle.
        static::creating(function (Customer $customer) {
            $customer->full_name = $customer->first_name.' '.$customer->last_name;
        });
    }

    /**
     * Relationships
     */
    public function bookings(): HasMany
    {
        return $this->hasMany(Booking::class, 'customer_id');
    }
}
