<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Booking extends Model
{
    use HasFactory, HasUuids;

    public $timestamps = true;

    protected $fillable = ['booking_id', 'agent_id', 'customer_id', 'museum_id'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Booking $booking) {
            $bookingNumber = self::getBookingNumber();
            $booking->booking_id = "CM-{$bookingNumber}";
        });
    }

    /**
     * Relationships
     */
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function museum(): BelongsTo
    {
        return $this->belongsTo(Museum::class, 'museum_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Generates a random number for booking.
     * This would either be an uuid with a human-readable alternative and handled by a dedicated class
     * or the BookingService in a microservice architecture.
     */
    public static function getBookingNumber(): int
    {
        do {
            $num = random_int(1, 99999);
            //Not usable in a production environment, just for demo purposes.
            //This query is not optimal in any way. I SUBSTR from every single row. Produces a table scan
            //and will be incredibly slow after a few thousand records.
            $result = DB::table('bookings')->whereRaw("CAST(SUBSTR(booking_id, 4) AS UNSIGNED) = {$num}")->count();
        } while ($result !== 0);

        return $num;
    }
}
