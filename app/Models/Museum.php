<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Museum extends Model
{
    use HasFactory, HasUuids;

    protected static function boot()
    {
        parent::boot();
        static::creating(function (Museum $museum) {
            //Not really any use case. Just wanted to prepend 'museum' because i used faker->name() in factory.
            $museum->name = "Museum {$museum->name}";
        });
    }

    protected $fillable = ['name', 'location'];

    public $timestamps = true;

    /**
     * Relationships
     */
    public function bookings(): HasMany
    {
        return $this->hasMany(Booking::class, 'museum_id');
    }
}
