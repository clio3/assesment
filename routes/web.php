<?php

use App\Http\Controllers\BookingsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
//Auth::routes(['reset' => false, 'register' => false, 'verify' => false]);

Route::get('/', function () {
    return redirect(route('login'), 301);
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/my-bookings', [BookingsController::class, 'index']);

require __DIR__.'/auth.php';
