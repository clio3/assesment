<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200" alt="Laravel Logo"></a></p>

## About The Project

Sample Project built for ClioMuseTours assessment.  
Built with the Laravel Framework (v10.37).  
I found this project a great opportunity to play around with the below libraries for the first time.

Libraries used:
> - [Yajra Datatables](https://github.com/yajra/laravel-datatables) Server Side implementation of jQuery Datatables
---

## Setup
Clone this repository
> git clone https://gitlab.com/clio3/assessment.git && cd assessment

Run composer to install php dependencies
> composer install
 
Set application encryption key
> php artisan key:generate

Run npm to install JS dependencies
> npm install && npm run build

Copy the example .env file
> cp example.env .env

Edit mysql connection settings
>DB_CONNECTION=mysql  
>DB_HOST=127.0.0.1  
>DB_PORT=3306  
>DB_DATABASE=assesment_kostikas  
>DB_USERNAME=root  
>DB_PASSWORD=

Run migrations and seed:
> php artisan migrate --seed

Setup complete. Now depending on your environment you can run the in-built server or visit your desired URL
> php artisan serve

Login using username: **CM-George**

---

## High level approach to SSO.
The following libraries are the base of SSO functionality regardless of the SSO provider.  

Start by installing [Spatie-Permissions](https://spatie.be/docs/laravel-permission/v6/introduction). Providing Role/Permission functionality with automatic guards.  

Auth2 login/register is an **incredibly** complex flow. For this reason alone I would avoid rolling one of my own and use  [Laravel Socialite](https://laravel.com/docs/10.x/socialite), a battle-tested official Laravel library provides a somewhat easy way to use SSO on your applications
 with a multitude of user-made providers.

One problem we may encounter is the syncing of roles/permissions between the provider and our application. We could avoid this by not using Spatie-Permissions and only use the ones provide by the SSO Provider, but we will lose that sweet developer experience, this package provides auto-completion for us.  

Another solution is to use the REST endpoints provided by the providers to fetch the roles/permissions and keep them in sync, and use the autocomplete functionality. 

Each user's role is always returned by the authentication flow in some form which we can keep for the session to authorize user actions. 

This is a rather simplified approach which would be enough in 99% of situations.

---
## Microservice Architecture

You can find this image in the repo called "Microservices Design.png".
![Image](Microservice_design.png)

Not necessarily an application design but more like a system design for a more complete look.

### The Frontend
One challenge of microservices UI is having a fluid UX on the frontend side, one solution could be microfrontends powered by an SPA.
Laravel is paired extremely well with VueJS. 

The styles would be served by the CDN as they are static and universal.  
De-coupling logic is the main advantage of microservices, by having each service hold it's own components would allow us to only serve them when needed on requests thus allowing a fluid, fast and responsive UX.
This could also be sped up more by using pre-connect and or a technology like turbo-links.  

This would also be the main component of the application connecting to the authentication service. API Getaway will handle the rest, I will talk about it in a while.

### The backend
By breaking up the application into many services allows de-coupling and better control of each service, while also providing better and easier deployments.

Starting with the **API Getaway**:  
It's main role is to forward each request to the correct service while also handling authentication by forwarding auth headers.  
Each request will go through the **cache**, allowing faster response times when it's a hit.

### Services
Each service will have it's own database and replica while also having regular backups.  
Handling its own logic and data without a care in the world for the rest.  
Inner-service connectivity and be a mix of event-based with a message broker like kafka or redis and HTTP Requests for immediate response.

### Misc services

**Logging:**  
Logging can be hard in a microservice architecture, there are multitude of services providing aggregators for us, sites like datadog and loggly can provide useful statistics and log retention.

**Monitoring**:
The more monitoring the better. Application uptime and health and my #1 priority. Services like [Sentry](https://sentry.io)
are log aggregators for crashes, providing instant insight on the application state and why it broke 

**Storage**: 
Cloud storage nowadays is a cheap solution to store files, most of them also provide CDNs for instant serving to the customer.  

**Searching**:
Not that useful in a ERP application but since the frontend will need it, it can be paired fairly well.

  

### The End
Thank you for your time and consideration.




