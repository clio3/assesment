
@extends('layouts.app')

@section('title', "{$breadcrumbs}"  )
@section('plugins.Datatables', true)

@section('content')
    <div class="container pt-5">
        <div class="card">
            <div class="card-header">

                <div class="card-title">
                    {{trans('My Bookings')}}

                </div>
            </div>
            <div class="card-body">

                {{ $dataTable->table([], $withFooter = true) }}
            </div>
        </div>

    </div>



@stop

@section('css')
@stop





@push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
@endpush

@section('js')
    <script> console.log('Hi!'); </script>
@stop
