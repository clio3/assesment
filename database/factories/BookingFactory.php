<?php

namespace Database\Factories;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\Museum;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Str;

class BookingFactory extends Factory
{
    protected $model = Booking::class;

    public function definition(): array
    {
        return [
            'booking_id' => Str::random(4),
            'customer_id' => Customer::factory(),
            'museum_id' => Museum::factory(),
            'user_id' => Str::uuid(),
            'created_at' => Carbon::now()->subWeeks(random_int(1, 20))->subDays(random_int(0, 7))->toDateTimeString(),
            'updated_at' => Carbon::now()->subWeeks(random_int(1, 20))->subDays(random_int(0, 7))->toDateTimeString(),
        ];
    }
}
