<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\User::factory()->create([
            'username' => 'CM-George',
            'email' => 'cmgeorge@cliomuseapp.com',
            'password' => null,
        ]);
    }
}
